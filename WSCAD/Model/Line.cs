﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSCAD.Model
{
    public class Line : Primitive
    {
        float A1 { get; set; }
        float A2 { get; set; }
        float B1 { get; set; }
        float B2 { get; set; }
    }
}
