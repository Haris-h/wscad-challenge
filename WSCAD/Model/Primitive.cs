﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace WSCAD.Model
{
    public class Primitive
    {
        public int Type { get; set; }
        public Color Color { get; set; }
        public int LineType { get; set; }
    }
}
