﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSCAD.Model
{
    public class Triangle : Primitive
    {
        float A1 { get; set; }
        float A2 { get; set; }
        float B1 { get; set; }
        float B2 { get; set; }
        float C1 { get; set; }
        float C2 { get; set; }
        bool Filled { get; set; }
    }
}
