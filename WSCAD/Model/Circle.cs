﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSCAD.Model
{
    public class Circle : Primitive
    {
        float Center1 { get; set; }
        float Center2 { get; set; }
        float Radius { get; set; }
        bool Filled { get; set; }
    }
}
