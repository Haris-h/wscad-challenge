﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WSCAD
{
    /// <summary>
    /// Includes functions for Main Task 2. 
    /// </summary>
    public static class PrimitivesHelper
    {
        public static Line GetLine(Point a, Point b, SolidColorBrush brushColor, DoubleCollection lineType, string tooltip)
        {
            var shape = new Line();

            shape.X1 = a.X;
            shape.X2 = a.Y;

            shape.Y1 = b.X;
            shape.Y2 = b.Y;
            shape.Fill = brushColor;
            shape.Stroke = brushColor;
            //shape.StrokeThickness = 2;

            shape.StrokeDashArray = lineType;
            shape.HorizontalAlignment = HorizontalAlignment.Center;
            shape.VerticalAlignment = VerticalAlignment.Center;
            shape.ToolTip = new ToolTip { Content = tooltip, Visibility = Visibility.Collapsed };
            shape.MouseLeftButtonDown += MainWindow.ShowElementDetails;

            return shape;
        }
        public static Ellipse GetCircle(Point center, double radius, SolidColorBrush brushColor, bool filled, DoubleCollection lineType, string tooltip)
        {
            var shape = new Ellipse();
            shape.Height = center.X + radius;
            shape.Width = center.Y + radius;
            shape.Stroke = brushColor;
            //shape.StrokeThickness = 2;
            if (filled)
            {
                shape.Fill = brushColor;
            }
            else
            {
                shape.Fill = Brushes.Transparent;
            }

            shape.StrokeDashArray = lineType;
            shape.HorizontalAlignment = HorizontalAlignment.Center;
            shape.VerticalAlignment = VerticalAlignment.Center;
            shape.ToolTip = new ToolTip { Content = tooltip, Visibility = Visibility.Collapsed };
            shape.MouseLeftButtonDown += MainWindow.ShowElementDetails;

            return shape;
        }


        public static Polygon GetTriangle(Point a, Point b, Point c, SolidColorBrush brushColor, bool filled, DoubleCollection lineType, string tooltip)
        {
            var shape = new Polygon();
            shape.Points.Add(a);
            shape.Points.Add(b);
            shape.Points.Add(c);

            shape.Stroke = brushColor;
            //shape.StrokeThickness = 2;
            if (filled)
            {
                shape.Fill = brushColor;
            }
            else
            {
                shape.Fill = Brushes.Transparent;
            }
            shape.StrokeDashArray = lineType;
            shape.HorizontalAlignment = HorizontalAlignment.Center;
            shape.VerticalAlignment = VerticalAlignment.Center;
            shape.ToolTip = new ToolTip { Content = tooltip, Visibility = Visibility.Collapsed };
            shape.MouseLeftButtonDown += MainWindow.ShowElementDetails;

            return shape;
        }
        public static Rectangle GetRectangle(double width, double height, SolidColorBrush brushColor, bool filled, DoubleCollection lineType, string tooltip)
        {
            var shape = new Rectangle();

            shape.Height = height;
            shape.Width = width;
            shape.Stroke = brushColor;
            //shape.StrokeThickness = 2;
            if (filled)
            {
                shape.Fill = brushColor;
            }
            else
            {
                shape.Fill = Brushes.Transparent;
            }
            shape.MouseLeftButtonDown += MainWindow.ShowElementDetails;
            shape.StrokeDashArray = lineType;
            shape.HorizontalAlignment = HorizontalAlignment.Center;
            shape.VerticalAlignment = VerticalAlignment.Center;
            shape.ToolTip = new ToolTip { Content = tooltip, Visibility = Visibility.Collapsed };
            return shape;
        }
    }
}
