﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Xml;

namespace WSCAD
{
    public static class DataHelper
    {
        /// <summary>
        /// Gets absolute path for json Example file.
        /// </summary>
        /// <returns></returns>
        public static string GetJsonExampleFilePath()
        {
            var root = AppDomain.CurrentDomain.BaseDirectory;
            var filePath = @"..\..\Data\example.json";

            return System.IO.Path.Combine(root, filePath);
        }
        /// <summary>
        /// Main task 1. 
        /// Used to read all shapes from the json example file and add it to the main windows grid.  
        /// Limited to the following shapes: 
        /// line, circle, triangle, rectangle.
        /// All other shapes will be ignored. 
        /// </summary>
        /// <param name="grid">Parent UIElement to which we add other shapes.</param>
        public static void ReadFromJsonFile(Grid grid)
        {
            var filePath = GetJsonExampleFilePath();
            using (StreamReader r = new StreamReader(filePath))
            {
                var json = r.ReadToEnd();
                dynamic items = JsonConvert.DeserializeObject(json);
                foreach (var item in items)
                {
                    var primitiveType = item["type"];
                    if (!primitiveType.HasValues && string.IsNullOrEmpty(primitiveType?.ToString()))
                        return;
                    //Main Task 2. 
                    switch (primitiveType?.ToString())
                    {
                        case "line":
                            grid.Children.Add(PrimitivesJsonHelper.GetLineFromJson(item));
                            break;
                        case "circle":
                            grid.Children.Add(PrimitivesJsonHelper.GetCircleFromJson(item));
                            break;
                        case "triangle":
                            grid.Children.Add(PrimitivesJsonHelper.GetTriangleFromJson(item));
                            break;
                        case "rectangle":
                            grid.Children.Add(PrimitivesJsonHelper.GetRectangleFromJson(item));
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        public static string GetXmlExampleFilePath()
        {
            var root = AppDomain.CurrentDomain.BaseDirectory;
            var filePath = @"..\..\Data\example.xml";

            return System.IO.Path.Combine(root, filePath);
        }
        /// <summary>
        /// Extra Tasks 2. 
        /// Used to read all shapes from the XML example file and add it to the main windows grid.  
        /// Limited to the following shapes: 
        /// line, circle, triangle, rectangle.
        /// All other shapes will be ignored. 
        /// </summary>
        /// <param name="grid">Parent UIElement to which we add other shapes.</param>
        public static void ReadFromXmlFile(Grid grid)
        {
            var filePath = GetXmlExampleFilePath();

            var doc = new XmlDocument();
            doc.Load(filePath);
            XmlNodeList xnList = doc.SelectNodes("/Primitives/Primitive");
            foreach (XmlNode item in xnList)
            {
                var primitiveType = item["type"].InnerText;
                if (string.IsNullOrEmpty(primitiveType?.ToString()))
                    return;
                //Main Task 2. 
                switch (primitiveType?.ToString())
                {
                    case "line":
                        grid.Children.Add(PrimitivesXmlHelper.GetLineFromXml(item));
                        break;
                    case "circle":
                        grid.Children.Add(PrimitivesXmlHelper.GetCircleFromXml(item));
                        break;
                    case "triangle":
                        grid.Children.Add(PrimitivesXmlHelper.GetTriangleFromXml(item));
                        break;
                    case "rectangle":
                        grid.Children.Add(PrimitivesXmlHelper.GetRectangleFromXml(item));
                        break;
                    default:
                        break;
                }
            }
        }
      
    }
}
