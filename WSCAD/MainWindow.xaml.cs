﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Windows.Markup;
using System.Drawing.Printing;
using System.Windows.Xps;
using System.Windows.Xps.Packaging;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace WSCAD
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void JsonButton_Click(object sender, RoutedEventArgs e)
        {
            ResultGrid.Children.Clear();
            DataHelper.ReadFromJsonFile(ResultGrid);
        }
        private void XmlButton_Click(object sender, RoutedEventArgs e)
        {
            ResultGrid.Children.Clear();
            DataHelper.ReadFromXmlFile(ResultGrid);
        }
        private void ClearResultGridButton_Click(object sender, RoutedEventArgs e)
        {
            ResultGrid.Children.Clear();
        }
        /// <summary>
        /// Exporting all content inside the ResultGrid. The ResultGrid covers the whole area underneath main menu. 
        /// This function uses a 3rd party library for PDF Export. First we create an XPS document, fill in the ResultGrid content and convert the XPS doc to a PDF. 
        /// </summary>
        /// <param name="sender">Navigation button.</param>
        /// <param name="e"></param>
        private void ExportGridToPdfButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog pd = new SaveFileDialog
            {
                DefaultExt = ".pdf",
                Filter = "PDF Documents (.pdf)|*.pdf"
            };

            bool? result = pd.ShowDialog();

            if (result != true) return;

            using (MemoryStream lMemoryStream = new MemoryStream())
            {
                Package package = Package.Open(lMemoryStream, FileMode.Create);
                XpsDocument doc = new XpsDocument(package);
                XpsDocumentWriter writer = XpsDocument.CreateXpsDocumentWriter(doc);
                writer.Write(ResultGrid);
                doc.Close();
                package.Close();

                using (PdfSharp.Xps.XpsModel.XpsDocument pdfXpsDoc = PdfSharp.Xps.XpsModel.XpsDocument.Open(lMemoryStream))
                {

                    PdfSharp.Xps.XpsConverter.Convert(pdfXpsDoc, pd.FileName, 0);
                    MessageBoxResult messageBox = MessageBox.Show("Your PDF file has been exported.", "PDF Export",
                                        MessageBoxButton.OK,
                                        MessageBoxImage.Information);

                }
            }

        }
        /// <summary>
        /// Showing a popup with details about the clicked primitive. Currently showing: Primitive type (Line, Circle, Triangle, Rectangle), Points/Coordiantes, Color (RGBA) and Line type.
        /// </summary>
        /// <param name="sender">Selected primitive</param>
        /// <param name="e"></param>
        public static void ShowElementDetails(object sender, RoutedEventArgs e)
        {
            MainWindow myWindow = Application.Current.MainWindow as MainWindow;
            var shape = sender as FrameworkElement;

            myWindow.PopupDetails.PlacementTarget = shape;
            if (myWindow.PopupDetails.IsOpen)
                myWindow.PopupDetails.IsOpen = false;
            else
            {
                var properties = shape.ToolTip.ToString().Split('&');
                myWindow.PopupTextType.Text = properties[1];
                myWindow.PopupTextPoints.Text = properties[2];
                myWindow.PopupTextColor.Text = properties[3];
                myWindow.PopupTextLineType.Text = properties[4];
                myWindow.PopupDetails.IsOpen = true;
            }
        }
    }
}
