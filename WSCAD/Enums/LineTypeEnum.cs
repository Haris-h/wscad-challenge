﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WSCAD.Enums
{
    public enum LineTypeEnum
    {
        Solid,
        Dot,
        Dash,
        DashDot
    }
}
