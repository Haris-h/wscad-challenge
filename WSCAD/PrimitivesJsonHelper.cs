﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WSCAD
{
    /// <summary>
    /// Includes functions for Main Task 2. 
    /// </summary>
    public static class PrimitivesJsonHelper
    {
        /// <summary>
        /// Main Task 2.a.
        /// </summary>
        /// <param name="item">json object from the example file</param>
        /// <returns>UIElement of type Line</returns>
        public static Line GetLineFromJson(dynamic item)
        {
            var a = DataTypesHelper.GetPointFromString(item["a"].ToString());
            var b = DataTypesHelper.GetPointFromString(item["b"].ToString());
            var brushColor = DataTypesHelper.GetColorFromString(item["color"].ToString());
            var lineType = DataTypesHelper.GetStrokeDashByLineType(item["lineType"].ToString());
            string tooltip = "&Line&A: " + item["a"].ToString() + " B: " + item["b"].ToString() + "&Color " + item["color"].ToString() + "&Line type " + item["lineType"].ToString();
            return PrimitivesHelper.GetLine(a, b, brushColor, lineType, tooltip);
        }
        /// <summary>
        /// Main Task 2. b.
        /// </summary>
        /// <param name="item">json object from the example file</param>
        /// <returns>UIElement of type Circle</returns>
        public static Ellipse GetCircleFromJson(dynamic item)
        {
            var center = DataTypesHelper.GetPointFromString(item["center"].ToString());
            var radius = Convert.ToDouble(item["radius"].ToString());
            var brushColor = DataTypesHelper.GetColorFromString(item["color"].ToString());
            var filled = Convert.ToBoolean(item["filled"].ToString());
            var lineType = DataTypesHelper.GetStrokeDashByLineType(item["lineType"].ToString());
            string tooltip = "&Circle&Center: " + item["center"].ToString() + "Radius: " + item["radius"].ToString() + "&Color " + item["color"].ToString() + "&Line type " + item["lineType"].ToString();

            return PrimitivesHelper.GetCircle(center, radius, brushColor, filled, lineType, tooltip);
        }
        /// <summary>
        /// Main Task 2. c.
        /// </summary>
        /// <param name="item">json object from the example file</param>
        /// <returns>UIElement of type Triangle</returns>
        public static Polygon GetTriangleFromJson(dynamic item)
        {
            var a = DataTypesHelper.GetPointFromString(item["a"].ToString());
            var b = DataTypesHelper.GetPointFromString(item["b"].ToString());
            var c = DataTypesHelper.GetPointFromString(item["c"].ToString());
            var brushColor = DataTypesHelper.GetColorFromString(item["color"].ToString());
            var filled = Convert.ToBoolean(item["filled"].ToString());
            var lineType = DataTypesHelper.GetStrokeDashByLineType(item["lineType"].ToString());
            string tooltip = "&Triangle&A: " + item["a"].ToString() + " B: " + item["b"].ToString() + " C: " + item["c"].ToString() + "&Color " + item["color"].ToString() + "&Line type " + item["lineType"].ToString();

            return PrimitivesHelper.GetTriangle(a, b, c, brushColor, filled, lineType, tooltip);
        }
       
        /// <summary>
        /// Extra Task 1
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static Rectangle GetRectangleFromJson(dynamic item)
        {
            var width = Convert.ToDouble(item["width"].ToString());
            var height = Convert.ToDouble(item["height"].ToString());
            var brushColor = DataTypesHelper.GetColorFromString(item["color"].ToString());
            var filled = Convert.ToBoolean(item["filled"].ToString());
            var lineType = DataTypesHelper.GetStrokeDashByLineType(item["lineType"].ToString());
            string tooltip = "&Rectangle&Width: " + item["width"].ToString() + " Height: " + item["height"].ToString() + "&Color " + item["color"].ToString() + "&Line type " + item["lineType"].ToString();

            return PrimitivesHelper.GetRectangle(width, height, brushColor, filled, lineType, tooltip);
        }
    }
}
