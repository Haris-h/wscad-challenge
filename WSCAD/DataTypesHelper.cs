﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace WSCAD
{
    public static class DataTypesHelper
    {
        /// <summary>
        /// Helper function for  Main Task 3.a.
        /// </summary>
        /// <param name="input">Example: "127; 255; 0; 0"</param>
        /// <returns></returns>
        public static SolidColorBrush GetColorFromString(string input)
        {
            var result = input.Split(';');
            var a = Convert.ToByte(result[0]);
            var r = Convert.ToByte(result[1]);
            var g = Convert.ToByte(result[2]);
            var b = Convert.ToByte(result[3]);
            return new SolidColorBrush(Color.FromArgb(a, r, g, b));
        }
        /// <summary>
        /// Helper function for Main Task 3.b.
        /// </summary>
        /// <param name="input">example: "-1,5; 3,4"</param>
        /// <returns></returns>
        public static Point GetPointFromString(string input)
        {
            var item = input.Split(';');
            var x = Convert.ToDouble(item[0].Replace(",", "."));
            var y = Convert.ToDouble(item[1].Replace(",", "."));

            return new Point(x, y);
        }
        /// <summary>
        /// Main Task 3.c.
        /// </summary>
        /// <param name="lineType">Currently covering only dot, dash, dashDot or solid (default)</param>
        /// <returns></returns>
        public static DoubleCollection GetStrokeDashByLineType(string lineType)
        {
            switch (lineType)
            {
                case "dot":
                    return new DoubleCollection() { 1 };
                case "dash":
                    return new DoubleCollection() { 6 };
                case "dashDot":
                    return new DoubleCollection() { 2, 6, 2 };
                case "solid":
                default:
                    return new DoubleCollection() { };
            }
        }
    }
}
