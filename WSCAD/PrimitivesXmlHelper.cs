﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Shapes;
using System.Xml;

namespace WSCAD
{
    public static class PrimitivesXmlHelper
    {
        public static Line GetLineFromXml(XmlNode item)
        {
            var a = DataTypesHelper.GetPointFromString(item["a"].InnerText);
            var b = DataTypesHelper.GetPointFromString(item["b"].InnerText);
            var brushColor = DataTypesHelper.GetColorFromString(item["color"].InnerText);
            var lineType = DataTypesHelper.GetStrokeDashByLineType(item["lineType"].InnerText);
            string tooltip = "&Line&A: " + item["a"].InnerText + " B: " + item["b"].InnerText + "&Color " + item["color"].InnerText + "&Line type " + item["lineType"].InnerText;

            return PrimitivesHelper.GetLine(a, b, brushColor, lineType, tooltip);
        }
        public static Ellipse GetCircleFromXml(XmlNode item)
        {
            var center = DataTypesHelper.GetPointFromString(item["center"].InnerText);
            var radius = Convert.ToDouble(item["radius"].InnerText);
            var brushColor = DataTypesHelper.GetColorFromString(item["color"].InnerText);
            var filled = Convert.ToBoolean(item["filled"].InnerText);
            var lineType = DataTypesHelper.GetStrokeDashByLineType(item["lineType"].InnerText);
            string tooltip = "&Circle&Center: " + item["center"].InnerText + "Radius: " + item["radius"].InnerText + "&Color " + item["color"].InnerText + "&Line type " + item["lineType"].InnerText;

            return PrimitivesHelper.GetCircle(center, radius, brushColor, filled, lineType, tooltip);
        }
        public static Polygon GetTriangleFromXml(XmlNode item)
        {
            var a = DataTypesHelper.GetPointFromString(item["a"].InnerText);
            var b = DataTypesHelper.GetPointFromString(item["b"].InnerText);
            var c = DataTypesHelper.GetPointFromString(item["c"].InnerText);
            var brushColor = DataTypesHelper.GetColorFromString(item["color"].InnerText);
            var filled = Convert.ToBoolean(item["filled"].InnerText);
            var lineType = DataTypesHelper.GetStrokeDashByLineType(item["lineType"].InnerText);
            string tooltip = "&Triangle&A: " + item["a"].InnerText + "B: " + item["b"].InnerText + "C: " + item["c"].InnerText + "&Color " + item["color"].InnerText + "&Line type " + item["lineType"].InnerText;

            return PrimitivesHelper.GetTriangle(a, b, c, brushColor, filled, lineType, tooltip);
        }
        public static Rectangle GetRectangleFromXml(XmlNode item)
        {
            var width = Convert.ToDouble(item["width"].InnerText);
            var height = Convert.ToDouble(item["height"].InnerText);
            var brushColor = DataTypesHelper.GetColorFromString(item["color"].InnerText);
            var filled = Convert.ToBoolean(item["filled"].InnerText);
            var lineType = DataTypesHelper.GetStrokeDashByLineType(item["lineType"].InnerText);
            string tooltip = "&Rectangle&Width: " + item["width"].InnerText + " Height: " + item["height"].InnerText + "&Color " + item["color"].InnerText + "&Line type " + item["lineType"].InnerText;

            return PrimitivesHelper.GetRectangle(width, height, brushColor, filled, lineType, tooltip);
        }
    }
}
